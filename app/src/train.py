import sys
import os

if not (len(sys.argv) == 3):
    print("Bad usage, expected: python train.py mnc/mnd/dc #iterations ")
    exit(1)

if sys.argv[1] == 'mnc':
    os.system("python mnist_train.py clothes " + sys.argv[2])
elif sys.argv[1] == 'mnd':
    os.system("python mnist_train.py digits " + sys.argv[2])
else:
    os.system("python cd_train.py " + sys.argv[2])
