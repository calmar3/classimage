import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)
import numpy as np
import os,glob,cv2
import sys,argparse
from os import listdir
from os.path import isfile, join
import time
ts = time.time()


# First, pass the path of the image
dir_path = os.path.dirname(os.path.realpath(__file__))

if not (len(sys.argv) == 3):
    print("Bad usage, expected: python predict.py /path_to_model /path_to_testing_set")
    exit(1)
model_path = sys.argv[1] #/model/dogs_cats_3x32_3x64_3x64__3x128_iterations100
test_path = sys.argv[2]  #/testing_data
testing_path = dir_path + test_path

classes = os.listdir(testing_path)
files = []
for i in classes:
    files_for_class = [f for f in listdir(testing_path+'/'+i) if isfile(join(testing_path+'/'+i, f))]
    files.append(files_for_class)

results = []
all_labels = []
i = 0
## Let us restore the saved model
sess = tf.Session()

# Step-1: Recreate the network graph. At this step only graph is created.
saver = tf.train.import_meta_graph('/app/src' + model_path + '/model.meta')
# Step-2: Now let's load the weights saved using the restore method.
saver.restore(sess, tf.train.latest_checkpoint('.' + model_path + '/'))

# Accessing the default graph which we have restored
graph = tf.get_default_graph()


for files_for_class in files:
    for file in files_for_class:
        filename = testing_path + '/' + classes[i] + '/' + file
        image_size = 128
        num_channels = 3
        images = []
        image = None
        # Reading the image using OpenCV
        image = cv2.imread(filename)
        # Resizing the image to our desired size and preprocessing will be done exactly as done during training
        image = cv2.resize(image, (image_size, image_size),0,0, cv2.INTER_LINEAR)
        images.append(image)
        images = np.array(images, dtype=np.uint8)
        images = images.astype('float32')
        images = np.multiply(images, 1.0/255.0)
        #The input to the network is of shape [None image_size image_size num_channels]. Hence we reshape.
        x_batch = images.reshape(1, image_size,image_size,num_channels)

        # Now, let's get hold of the op that we can be processed to get the output.
        # In the original network y_pred is the tensor that is the prediction of the network
        y_pred = graph.get_tensor_by_name("y_pred:0")
        ## Let's feed the images to the input placeholders
        x = graph.get_tensor_by_name("x:0")
        y_true = graph.get_tensor_by_name("y_true:0")
        y_test_images = np.zeros((1, len(os.listdir('training_data'))))

        ### Creating the feed_dict that is required to be fed to calculate y_pred
        feed_dict_testing = {x: x_batch, y_true: y_test_images}
        result = sess.run(y_pred, feed_dict=feed_dict_testing)
        result = result.tolist()
        results.append(result[0])
        labels = [0] * len(classes)
        labels[i] = 1
        all_labels.append(labels)
    i += 1

# result is of this format [probabiliy_of_class1, probability_of_sunflower]
accuracy, update_op = tf.metrics.accuracy(labels=tf.argmax(all_labels, 1), predictions=tf.argmax(results, 1))
auc, update_auc = tf.metrics.auc(
    tf.convert_to_tensor(all_labels),
    tf.convert_to_tensor(results),
    weights=None,
    num_thresholds=200,
    metrics_collections=None,
    updates_collections=None,
    curve='ROC',
    name=None,
    summation_method='trapezoidal'
)

log_loss = tf.losses.log_loss(
    tf.convert_to_tensor(all_labels),
    tf.convert_to_tensor(results),
    weights=1.0,
    epsilon=1e-07,
    scope=None,
    loss_collection=tf.GraphKeys.LOSSES,
    reduction=tf.losses.Reduction.SUM_BY_NONZERO_WEIGHTS
)

precision, update_precision = tf.metrics.precision(
    tf.convert_to_tensor(all_labels),
    tf.convert_to_tensor(results),
    weights=None,
    metrics_collections=None,
    updates_collections=None,
    name=None
)

recall, update_recall = tf.metrics.recall(
    tf.convert_to_tensor(all_labels),
    tf.convert_to_tensor(results),
    weights=None,
    metrics_collections=None,
    updates_collections=None,
    name=None
)

sess.run(tf.local_variables_initializer())
sess.run(tf.global_variables_initializer())
final_accuracy_update = sess.run([accuracy, update_op])
final_accuracy = sess.run([accuracy])
final_auc_update = sess.run([auc, update_auc])
final_auc = sess.run([auc])
final_logloss = sess.run([log_loss])
final_precision_update = sess.run([precision, update_precision])
final_precision = sess.run([precision])
final_recall_update = sess.run([recall, update_recall])
final_recall = sess.run([recall])
print("Accuracy: " + str(final_accuracy[0]))
print("AUC: " + str(final_auc[0]))
print("Log Loss: " + str(final_logloss[0]))
print("Precision: " + str(final_precision[0]))
print("Recall: " + str(final_recall[0]))
f1_score = (2*(final_precision[0]*final_recall[0]))/(final_precision[0]+final_recall[0])
print("F1 Score: " + str(f1_score))
model_path = model_path.split("/")
model_path = model_path[len(model_path)-1]
with open('results/metrics.csv', 'ab') as f:
    f.write(model_path + ',' + str(final_accuracy[0])
            + ',' + str(final_auc[0]) + ',' + str(final_logloss[0])
            + ',' + str(final_recall[0]) + ',' + str(final_precision[0])
            + ',' + str(f1_score) + "\n")
