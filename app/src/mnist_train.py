import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)
import time
from datetime import timedelta
import math
import random
import numpy as np
import os
import sys
#from mlxtend.data import loadlocal_mnist

#Adding Seed so that random initialization is consistent
from numpy.random import seed
seed(1)
from tensorflow import set_random_seed
set_random_seed(2)
from tensorflow.examples.tutorials.mnist import input_data


mnist = input_data.read_data_sets("./data/" + sys.argv[1], one_hot=True)

import time

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


start = int(round(time.time() * 1000))

num_iterations = int(sys.argv[2])
sess = tf.InteractiveSession()

x = tf.placeholder(tf.float32, shape=[None, 784], name='x')
y_ = tf.placeholder(tf.float32, shape=[None, 10], name='y_')
keep_prob = tf.placeholder(tf.float32)


num_channels = 1
fc_layer_size = 1024
num_classes = 10
filter_size_conv1 = 5
num_filters_conv1 = 32

filter_size_conv2 = 5
num_filters_conv2 = 32

filter_size_conv3 = 5
num_filters_conv3 = 64

filter_size_conv4 = 3
num_filters_conv4 = 64

filter_size_conv5 = 3
num_filters_conv5 = 128

def create_weights(shape):
    return tf.Variable(tf.truncated_normal(shape, stddev=0.1))

def create_biases(size):
    return tf.Variable(tf.constant(0.1, shape=[size]))


def create_convolutional_layer(input,
                               num_input_channels,
                               conv_filter_size,
                               num_filters):
    ## We shall define the weights that will be trained using create_weights function.
    weights = create_weights(shape=[conv_filter_size, conv_filter_size, num_input_channels, num_filters])
    ## We create biases using the create_biases function. These are also trained.
    biases = create_biases(num_filters)

    ## Creating the convolutional layer
    layer = tf.nn.conv2d(input=input,
                         filter=weights,
                         strides=[1, 1, 1, 1],
                         padding='SAME')
    layer += biases


    ## We shall be using max-pooling.
    layer = tf.nn.max_pool(value=layer,
                           ksize=[1, 2, 2, 1],
                           strides=[1, 2, 2, 1],
                           padding='SAME')

    ## Output of pooling is fed to Relu which is the activation function for us.
    layer = tf.nn.relu(layer)

    return layer


def create_flatten_layer(layer):
    # We know that the shape of the layer will be [batch_size img_size img_size num_channels]
    # But let's get it from the previous layer.
    layer_shape = layer.get_shape()
    ## Number of features will be img_height * img_width* num_channels. But we shall calculate it in place of hard-coding it.
    num_features = layer_shape[1:4].num_elements()
    ## Now, we Flatten the layer so we shall have to reshape to num_features
    layer = tf.reshape(layer, [-1, num_features])
    return layer


def create_fc_layer(input,
                    drop,
                    num_inputs,
                    num_outputs,
                    use_relu=True):
    # Let's define trainable weights and biases.
    weights = create_weights(shape=[num_inputs, num_outputs])
    biases = create_biases(num_outputs)

    if drop:
        input = tf.nn.dropout(input,keep_prob)

    # Fully connected layer takes input x and produces wx+b.Since, these are matrices, we use matmul function in Tensorflow
    layer = tf.matmul(input, weights) + biases
    if use_relu:
        layer = tf.nn.relu(layer)

    return layer


x_image = tf.reshape(x, [-1, 28, 28, 1])

layer_conv1 = create_convolutional_layer(input=x_image,
                                         num_input_channels=num_channels,
                                         conv_filter_size=filter_size_conv1,
                                         num_filters=num_filters_conv1)

layer_conv2 = create_convolutional_layer(input=layer_conv1,
                                         num_input_channels=num_filters_conv1,
                                         conv_filter_size=filter_size_conv2,
                                         num_filters=num_filters_conv2)

layer_conv3 = create_convolutional_layer(input=layer_conv2,
                                         num_input_channels=num_filters_conv2,
                                         conv_filter_size=filter_size_conv3,
                                         num_filters=num_filters_conv3)

#layer_conv4 = create_convolutional_layer(input=layer_conv3,
#              num_input_channels=num_filters_conv3,
#              conv_filter_size=filter_size_conv4,
#              num_filters=num_filters_conv4)

#layer_conv5 = create_convolutional_layer(input=layer_conv4,
#              num_input_channels=num_filters_conv4,
#              conv_filter_size=filter_size_conv5,
#              num_filters=num_filters_conv5)

layer_flat = create_flatten_layer(layer_conv3)

layer_fc1 = create_fc_layer(input=layer_flat,
                            drop=False,
                            num_inputs=layer_flat.get_shape()[1:4].num_elements(),
                            num_outputs=fc_layer_size,
                            use_relu=True)

y_conv = create_fc_layer(input=layer_fc1,
                            drop=True,
                            num_inputs=fc_layer_size,
                            num_outputs=num_classes,
                            use_relu=False)


model = "mnist_" + sys.argv[1] + '_' + (str(filter_size_conv1) + 'x' + str(num_filters_conv1)
              + '_' + str(filter_size_conv2) + 'x' + str(num_filters_conv2)
              + '_' + str(filter_size_conv3) + 'x' + str(num_filters_conv3)
              #+ '_' + str(filter_size_conv4) + 'x' + str(num_filters_conv4)
              #+ '_' + str(filter_size_conv5) + 'x' + str(num_filters_conv5)
              + '_iterations' + str(num_iterations))

cross_entropy = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv))
train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))


with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for i in range(num_iterations):
        batch = mnist.train.next_batch(500)
        valid_batch = mnist.validation.next_batch(500)
        labels = batch[1]
        valid_labels = valid_batch[1]
        feed_dict_val = {x: valid_batch[0], y_: valid_labels, keep_prob: 1.0}
        if i % int(len(mnist.train.labels) / 500) == 0:
            epoch = int(i / int(len(mnist.train.labels) / 500))
            val_accuracy = accuracy.eval(feed_dict_val)
            train_accuracy = accuracy.eval(feed_dict={
                x: batch[0], y_: batch[1], keep_prob: 1.0})
            val_loss = sess.run(cross_entropy, feed_dict=feed_dict_val)
            msg = "Training Epoch {0} --- Training Accuracy: {1:>6.1%}, Validation Accuracy: {2:>6.1%},  Validation Loss: {3:.3f}"
            print(msg.format(epoch + 1, train_accuracy, val_accuracy, val_loss))
        train_step.run(feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})
    stop = int(round(time.time() * 1000))
    accuracy_value = accuracy.eval(feed_dict={x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0})
    print('Accuracy: ' + str(accuracy_value))
    prediction = tf.argmax(y_conv,1)
    result = prediction.eval(feed_dict={x:mnist.test.images,y_:mnist.test.labels,keep_prob:1.0})
    final_res = []
    for value in result:
        res = [0] * 10
        res[value] = 1
        final_res.append(res)
    auc, update_auc = tf.metrics.auc(
        tf.convert_to_tensor(mnist.test.labels),
        tf.convert_to_tensor(final_res),
        weights=None,
        num_thresholds=200,
        metrics_collections=None,
        updates_collections=None,
        curve='ROC',
        name=None,
        summation_method='trapezoidal'
    )

    log_loss = tf.losses.log_loss(
        tf.convert_to_tensor(mnist.test.labels),
        tf.convert_to_tensor(final_res),
        weights=1.0,
        epsilon=1e-07,
        scope=None,
        loss_collection=tf.GraphKeys.LOSSES,
        reduction=tf.losses.Reduction.SUM_BY_NONZERO_WEIGHTS
    )

    precision, update_precision = tf.metrics.precision(
        tf.convert_to_tensor(mnist.test.labels),
        tf.convert_to_tensor(final_res),
        weights=None,
        metrics_collections=None,
        updates_collections=None,
        name=None
    )

    recall, update_recall = tf.metrics.recall(
        tf.convert_to_tensor(mnist.test.labels),
        tf.convert_to_tensor(final_res),
        weights=None,
        metrics_collections=None,
        updates_collections=None,
        name=None
    )

    sess.run(tf.local_variables_initializer())
    sess.run(tf.global_variables_initializer())
    final_auc_update = sess.run([auc, update_auc])
    final_auc = sess.run([auc])
    final_logloss = sess.run([log_loss])
    final_precision_update = sess.run([precision, update_precision])
    final_precision = sess.run([precision])
    final_recall_update = sess.run([recall, update_recall])
    final_recall = sess.run([recall])
    print("AUC: " + str(final_auc[0]))
    print("Log Loss: " + str(final_logloss[0]))
    print("Precision: " + str(final_precision[0]))
    print("Recall: " + str(final_recall[0]))
    f1_score = (2 * (final_precision[0] * final_recall[0])) / (final_precision[0] + final_recall[0])
    print("F1 Score: " + str(f1_score))
    with open('results/performance.csv', 'ab') as f:
        f.write(model
                + ',' + str(stop - start)
                + ',' + str(train_accuracy) + ',' + str(val_accuracy)
                + ',' + str(val_loss) + '\n')
    with open('results/metrics.csv', 'ab') as f:
        f.write(model
                + ',' + str(accuracy_value)
                + ',' + str(final_auc[0]) + ',' + str(final_logloss[0])
                + ',' + str(final_recall[0]) + ',' + str(final_precision[0])
                + ',' + str(f1_score) + "\n")
