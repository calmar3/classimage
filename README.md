
## Classimage

Tensorflow Python application to train and test CNN's images classifier


---------------------------------------------------------------------------------------

### Configuration

Install `Docker`

Download the repository and the dataset from [Google Drive](https://drive.google.com/file/d/1fg8m-zUstObHk_KwE7b8kNFNtvQ3fd6b/view)

Extract the archive and copy `data`, `training_data` and `testing_data` directories in `src` project's directory

---------------------------------------------------------------------------------------

### Build

Open a terminal window in the project directory and run:  `./build.sh`

---------------------------------------------------------------------------------------

### Run

From the terminal in project directory run:  `./start.sh` 

Now you're logged into a Docker container and you can run the project:

    - Run `./make_train.sh` to start a training operation. Check the content of this script for the correctness of parameters
    - Run `./make_predict.sh` to start a testing operation. Check the content of this script for the correctness of parameters
 